package main

import (
	"fmt"
	gw "github.com/hooksie1/vault-grpc/gen/go/vault/service/v1"
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	listener , err := net.Listen("tcp", ":9090")
	if err != nil {
		fmt.Println(err)
	}

	s := gw.Server{}
	gs := grpc.NewServer()

	gw.RegisterSecretRetrieverServer(gs, &s)

	if err := gs.Serve(listener); err != nil {
		log.Fatal(err)
	}
}
