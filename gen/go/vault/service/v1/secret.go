package v1

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"google.golang.org/grpc/metadata"
	"io"
	"net/http"
	"os"
	"time"
)


type KV struct {
	RequestID     string `json:"request_id"`
	LeaseID       string `json:"lease_id"`
	Renewable     bool   `json:"renewable"`
	LeaseDuration int    `json:"lease_duration"`
	Data          struct {
		Data struct {
			Password string `json:"password"`
		} `json:"data"`
		Metadata struct {
			CreatedTime  time.Time `json:"created_time"`
			DeletionTime string    `json:"deletion_time"`
			Destroyed    bool      `json:"destroyed"`
			Version      int       `json:"version"`
		} `json:"metadata"`
	} `json:"data"`
	WrapInfo interface{} `json:"wrap_info"`
	Warnings interface{} `json:"warnings"`
	Auth     interface{} `json:"auth"`
}

type JWTAuth struct {
	RequestID     string      `json:"request_id"`
	LeaseID       string      `json:"lease_id"`
	Renewable     bool        `json:"renewable"`
	LeaseDuration int         `json:"lease_duration"`
	Data          interface{} `json:"data"`
	WrapInfo      interface{} `json:"wrap_info"`
	Warnings      interface{} `json:"warnings"`
	Auth          struct {
		ClientToken   string   `json:"client_token"`
		Accessor      string   `json:"accessor"`
		Policies      []string `json:"policies"`
		TokenPolicies []string `json:"token_policies"`
		Metadata      struct {
			Role string `json:"role"`
		} `json:"metadata"`
		LeaseDuration int    `json:"lease_duration"`
		Renewable     bool   `json:"renewable"`
		EntityID      string `json:"entity_id"`
		TokenType     string `json:"token_type"`
		Orphan        bool   `json:"orphan"`
	} `json:"auth"`
}


type Server struct {}

func (s *Server) mustEmbedUnimplementedSecretRetrieverServer() {}

func (s *Server) GetSecret(ctx context.Context, sr *SecretRequest) (*SecretResponse, error) {
	var kv KV
	var secret SecretResponse

	client := http.Client{}

	req, err := buildRequest(ctx, sr.Path, "GET")
	if err != nil {
		return nil, err
	}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	buf := bytes.Buffer{}
	io.Copy(&buf, resp.Body)
	data := buf.Bytes()

	if err := json.Unmarshal(data, &kv); err != nil {
		return nil, err
	}


	secret.SecretValue = kv.Data.Data.Password

	return &secret, nil


}

func buildRequest(ctx context.Context, path, method string) (*http.Request, error) {
	metadata, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, fmt.Errorf("Error getting context")
	}

	jwt := metadata["x-vault-jwt"][0]
	secret := metadata["x-vault-secret"][0]

	token, err := retrieveVaultToken(jwt)
	if err != nil {
		return nil, err
	}

	url := fmt.Sprintf("%s/v1/%s/data/%s", os.Getenv("VAULT_ADDR"), path, secret)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("X-Vault-Token", token)
	if metadata["x-vault-namespace"] != nil {
		req.Header.Add("X-Vault-Namespace", metadata["x-vault-namespace"][0])
	}

	return req, nil

}

func retrieveVaultToken(jwt string) (string, error) {
	url := fmt.Sprintf("%s/v1/auth/jwt/login", os.Getenv("VAULT_ADDR"))
	jwtmap := map[string]string {
		"jwt": jwt,
	}
	data, err := json.Marshal(jwtmap)
	if err != nil {
		return "", nil
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return "", nil
	}

	defer resp.Body.Close()

	buf := bytes.Buffer{}
	io.Copy(&buf, resp.Body)
	authData := buf.Bytes()


	var ja JWTAuth

	if err := json.Unmarshal(authData, &ja); err != nil {
		return "", err
	}

	return ja.Auth.ClientToken, nil


}